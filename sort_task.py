from task import *
from operator import attrgetter

def sort(tasks, type = "id"):  
    return sorted(tasks, key=attrgetter(type))
        
##tasks =[
##task(0, "pomme", 'A'),
##task(1, "poire", 'B'),
##task(2, "peche", 'D'),
##task(3, "pamplemousse", 'E'),
##task(4, "papaye", 'P'),
##task(5, "pasteque", 'd'),
##task(6, "pistache", "Y"),
##task(7, "prune")]
##    
##print('----\n---')
##for task in sorted(tasks, key=lambda task: task.priority):
##    print(task)