##   ___   .                              .                  _             ##
## .'   \  |     ___    ____   ____      _/_     ___    ___  /        ___  ##
## |       |    /   `  (      (           |     /   ` .'   ` |,---. .'   ` ##
## |       |   |    |  `--.   `--.        |    |    | |      |'   ` |----' ##
##  `.__, /\__ `.__/| \___.' \___.'       \__/ `.__/|  `._.' /    | `.___, ##
##                                                                         ##

class task:
    def __init__ (self, id, text, priority = "", completion = "", title = "", completion_date = "", creation_date = "", due_date = "", project_tag = [], context_tag = [], meta = {}):

        ### Cas simple ###
        self.text               =  self.__ensure_type__(text,                str  )
        self.id                 =  self.__ensure_type__(id,                  int  )
        self.priority           =  self.__ensure_prio__(priority                  )
        self.title              =  self.__ensure_type__(title,               str  )
        self.due_date           =  self.__ensure_type__(due_date,            str  )
        self.project_tag        =  self.__ensure_type__(project_tag,         list )
        self.context_tag        =  self.__ensure_type__(context_tag,         list )
        self.meta               =  self.__ensure_type__(meta,                dict )

        ### Constantes utiles (privées) ###
        self.__date_format             =  "%Y-%m-%d"
        self.__COMPLETED               =  "x "
        ##self.__ascii_uppercase         =  "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        self.__texte_originale         =  self.text
        self.__priorite_originale      =  self.priority

        ### Cas qui ont besoin de ###

        ### Tache finie ? ###
        self.__ensure_type__(completion,            str  )
        completion = completion.strip().lower()
        if completion == self.__COMPLETED.strip():
            self.completion     =  completion
            __completed = True
        else:
            self.completion     =  ""
            __completed = False

        ### Date de fin ###
        if completion_date == "" and __completed == True:
            self.complete_date()
        else:
            self.completion_date=  self.__ensure_type__(completion_date,     str  )

        ### Date de debut ###
        if creation_date == "":
            self.create_date()
        else:
            self.creation_date  =  self.__ensure_type__(creation_date,       str  )

    def __repr__(self):
        d={'id': self.id, 'text': self.text}
        d['priority'] = self.priority 
        d['completion'] = self.completion
        d['title'] = self.title
        d['completion_date'] = self.completion_date
        d['creation_date'] = self.creation_date
        d['due_date'] = self.due_date
        d['project_tag'] = self.project_tag
        d['context_tag'] = self.context_tag
        d['meta'] = self.meta
        for key in list(d.keys()):
            element = d[key]
            if type(element) == int:
                element = str(element)
            if len(element) == 0:
                del d[key]
        return str(d)
    
    ###                              ###
    ### Methodes propres à la classe ###
    ###                              ###

    def uncomplete(self):
        self.completion = ""
        self.complete_date('')

    def complete(self):
        self.completion = self.__COMPLETED
        self.complete_date()

    def prioritize(self, prio = "A"):
        self.__ensure_type__(prio, str)
        self.priority = prio

    def update(self, texte):
        self.__ensure_type__(texte, str)
        self.texte = texte

    ###On prefere ici effectuer une verification qui peu sembler inutile, pour ne pas importer datetime directement dans la classe alors qu'il n'est utile qu'ici
    
    def complete_date(self, date= -1):
        if date == -1:
            import datetime
            date = datetime.datetime.now().strftime(self.__date_format)
        self.__ensure_type__(date, str)
        self.completion_date = date

    def create_date(self, date= -1):
        if date == -1:
            import datetime
            date = datetime.datetime.now().strftime(self.__date_format)
        self.__ensure_type__(date, str)
        self.creation_date = date

    def add_due_date(self, date):
        self.__ensure_date__(date, self.__date_format)
        self.due_date = date

    def add_title(self, title):
        self.__ensure_type__(title, str)
        self.title.append(title)

    def add_context(self, context):
        self.__ensure_type__(context, str)
        self.context_tag.append(context)

    def add_project(self, project):
        self.__ensure_type__(project, str)
        self.project_tag.append(project)

    def add_meta(self, project):
        self.__ensure_type__(project, str)
        self.project_tag.append(project)

    ### Methode de verifiation de type et de format ###
    
    def __ensure_type__(self, value, types):
        if isinstance(value, types):
            return value
        else:
            raise TypeError('La valeur {value} est {value_type}, mais devrait être de type {types}'.format(value=value, value_type=type(value), types=types))

    def __ensure_date__(self, date, format):
        self.__ensure_type__(date, str)
        try:
            import datetime
            datetime.datetime.strptime(date, format)
        except ValueError:
            raise ValueError('La date {date} n\'est pas au format {format}'.format(date=date, format=format))

    def __ensure_prio__(self, prio):
        self.__ensure_type__(prio, str)
        prio = prio.strip().lower()
        ascii_uppercase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.lower()
        if prio in ascii_uppercase:
            return prio
        return ' '

    ### Methode unparse, on aurait pu la mettre dans to-do, mais c'est sympa de la mettre en methode et non en fonction, ça fait classe
    
    def unparse_dict(self):
        tache = {}
        if self.completion != "":
            tache['completion'] = self.completion

        if self.priority.strip() != "":
            tache['priority'] = self.priority

        if self.completion_date.strip() != "":
            tache['completion_date'] = self.completion_date

        if self.creation_date.strip() != "":
            tache['creation_date'] = self.creation_date

        if self.text.strip() != "":
            tache['text'] = self.text

        if self.title.strip() != "":
            tache['title'] = self.title

        if len(self.project_tag) != 0:
            tache['project_tag'] = self.project_tag

        if len(self.context_tag) != 0:
            tache['context_tag'] = self.context_tag

        if len(self.meta) != 0:
            tache['meta'] = self.meta

        if self.due_date.strip() != "":
            tache['due_date'] = self.due_date

        return tache

    def unparse(self):
        tache = ""
        if self.completion != "":
            tache += self.completion
            tache += " "

        if self.priority.strip() != "":
            print(self.priority)
            tache += "(" + self.priority + ") "
            tache += " "

        if self.completion_date.strip() != "":
            tache += self.completion_date
            tache += " "

        if self.creation_date.strip() != "":
            tache += self.creation_date
            tache += " "

        if self.text.strip() != "":
            tache += self.text
            tache += " "

        if self.title.strip() != "":
            tache += self.title
            tache += " "

        if len(self.project_tag) != 0:
            tache += "+" + str(self.project_tag).strip()
            tache += " "

        if len(self.context_tag) != 0:
            tache += "@" + str(self.context_tag).strip()
            tache += " "

        if len(self.meta) != 0:
            tache += "meta:" + str(self.meta).strip()
            tache += " "

        if self.due_date.strip() != "":
            tache += "due:" + self.due_date
            tache += " "

        return tache

##    ┌─┐┌─┐┬─┐┌┬┐    ##
##    └─┐│ │├┬┘ │     ##
##    └─┘└─┘┴└─ ┴     ##     

def sort_tasks(tasks, type = "id"):
    from operator import attrgetter
    return sorted(tasks, key=attrgetter(type))