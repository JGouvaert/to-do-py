##
## ██████╗  ██████╗ ██╗   ██╗██╗   ██╗ █████╗ ███████╗██████╗ ████████╗                                        ██████╗  ██████╗  ██╗███████╗
##██╔════╝ ██╔═══██╗██║   ██║██║   ██║██╔══██╗██╔════╝██╔══██╗╚══██╔══╝                                        ╚════██╗██╔═████╗███║╚════██║
##██║  ███╗██║   ██║██║   ██║██║   ██║███████║█████╗  ██████╔╝   ██║                                            █████╔╝██║██╔██║╚██║    ██╔╝
##██║   ██║██║   ██║██║   ██║╚██╗ ██╔╝██╔══██║██╔══╝  ██╔══██╗   ██║                                           ██╔═══╝ ████╔╝██║ ██║   ██╔╝
##╚██████╔╝╚██████╔╝╚██████╔╝ ╚████╔╝ ██║  ██║███████╗██║  ██║   ██║                                           ███████╗╚██████╔╝ ██║   ██║
## ╚═════╝  ╚═════╝  ╚═════╝   ╚═══╝  ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝   ╚═╝                                           ╚══════╝ ╚═════╝  ╚═╝   ╚═╝
## ██████╗  ██████╗  ██████╗ ███████╗███████╗███████╗███╗   ██╗███████╗                                        ██████╗  ██████╗  ██╗ █████╗
##██╔════╝ ██╔═══██╗██╔═══██╗██╔════╝██╔════╝██╔════╝████╗  ██║██╔════╝                                        ╚════██╗██╔═████╗███║██╔══██╗
##██║  ███╗██║   ██║██║   ██║███████╗███████╗█████╗  ██╔██╗ ██║███████╗                                         █████╔╝██║██╔██║╚██║╚█████╔╝
##██║   ██║██║   ██║██║   ██║╚════██║╚════██║██╔══╝  ██║╚██╗██║╚════██║                                        ██╔═══╝ ████╔╝██║ ██║██╔══██╗
##╚██████╔╝╚██████╔╝╚██████╔╝███████║███████║███████╗██║ ╚████║███████║                                        ███████╗╚██████╔╝ ██║╚█████╔╝
## ╚═════╝  ╚═════╝  ╚═════╝ ╚══════╝╚══════╝╚══════╝╚═╝  ╚═══╝╚══════╝                                        ╚══════╝ ╚═════╝  ╚═╝ ╚════╝
##████████╗ ██████╗ ██████╗  ██████╗       ██╗     ██╗███████╗████████╗                                         █████╗ ██████╗  ██╗
##╚══██╔══╝██╔═══██╗██╔══██╗██╔═══██╗      ██║     ██║██╔════╝╚══██╔══╝                                        ██╔══██╗██╔══██╗███║
##   ██║   ██║   ██║██║  ██║██║   ██║█████╗██║     ██║███████╗   ██║                                           ███████║██████╔╝╚██║
##   ██║   ██║   ██║██║  ██║██║   ██║╚════╝██║     ██║╚════██║   ██║                                           ██╔══██║██╔═══╝  ██║
##   ██║   ╚██████╔╝██████╔╝╚██████╔╝      ███████╗██║███████║   ██║                                           ██║  ██║██║      ██║
##   ╚═╝    ╚═════╝ ╚═════╝  ╚═════╝       ╚══════╝╚═╝╚══════╝   ╚═╝                                           ╚═╝  ╚═╝╚═╝      ╚═╝
##

file_name= "to-do.txt"
ligne_defaut = "X (A) XXXX-XX-XX XXXX-XX-XX TXT +Tag_Projet @Tag_Context due:XXX-XX-XX #ti: Titre"
dict_defaut = {'N' : 1, 'X': 'X', 'completionDate': 'XXXX-XX-XX' ,'creationDate': 'XXXX-XX-XX', 'priority': 'A','dueDate': 'XXXX-XX-XX', 'context' : '', 'project':[], 'meta':{}, 'text': 'lorem ipsum','titre':'Title'}
###Permet de tester l'existence du fichier et de creer un fichier de base si todo n'existe pas
try:
    td = open(file_name, "xt")
    td.close()
    td = open(file_name, "wt")
    print("Bienvenu sur la to-do List")
    td.write(ligne_defaut + '\n')
    td.close()
except:
    print("Content de te revoir sur la to-do List")

##  ______               _   _                        _____  __  ##
## |  ____|             | | (_)                 /\   |  __ \/_ | ##
## | |__ ___  _ __   ___| |_ _  ___  _ __      /  \  | |__) || | ##
## |  __/ _ \| '_ \ / __| __| |/ _ \| '_ \    / /\ \ |  ___/ | | ##
## | | | (_) | | | | (__| |_| | (_) | | | |  / ____ \| |     | | ##
## |_|  \___/|_| |_|\___|\__|_|\___/|_| |_| /_/    \_\_|     |_| ##
##                                                               ##

def parser_tache(ligne = ligne_defaut):
    """
    Fonction à finir, ne pas toucher
    """
    ligne = ligne.split()
    return ligne

def unparser(tache = dict_defaut):
    """
    """
    return tache

def compare_date(d1,d2):
    """
    Renvoi 0 si les dates sont égales, -1 si D1 est avant D2, 1 si D1 est après D2
    """
    d1 = d1.split('-')
    d2 = d2.split('-')

    Da = int(d1[0]) - int(d2[0])
    Dm = int(d1[1]) - int(d2[1])
    Dj = int(d1[2]) - int(d2[2])

    if not (Da == Dm == Dj == 0):
        if Da != 0:
            Da = Da // abs(Da)
            ## Soit Da == 0 ou Da = -1 ou Da = 1

            return Da
        if Dm != 0:
            Dm = Dm // abs(Dm)
            ## Soit Dm == 0 ou Dm = -1 ou Dm = 1

            return Dm
        if Dj != 0:
            Dj = Dj // abs(Dj)
            ## Soit Dj == 0 ou Dj = -1 ou Dj = 1

            return Dj
    else:
        return 0

def tri_date(d1,d2):
    """
    Renvoi une liste (DateAprès, DateAvant)
    """
    date=compare_date(d1,d2)
    if date == 0 or date == 1:
        return (d1, d2)
    else:
        return (d2,d1)

def verifier_date(d1):
    """
Pour verifier les dates, inutiles puisque si tout ce passe bien, y'a pas besoin mais bon, c'est sympa on pourra le mettre dans le Log, au moins il sert a quelle que chose
    """
    date=d1.split('-')
    if len(date[0])==4:
        if 1 <= int(date[1]) <= 12:
            if 1 <= int(date[1]) <= 31:
                return 1
    else:
        return -1

def pretty(task):
    """
    Fonction à finir, ne pas toucher
    Fonction qui à pour but d'afficher proprement en console une tache

    :param tache: (dict) Un dictionnaire TACHE
    :return: Aucun

    :CU: Aucune
    """
    try:
        print("---------------------------------------")
        for element in task:
            print( element, task[element])
    except Exception as e:
        log(str(e))

def lire(fichier = file_name):
    """
    Fonction de lecture du fichier, renvoi la liste des taches contenues dans le fichier

    :param fichier: (str) Valeur par defaut: to-do.txt
    :return: (list) La liste des tâches dans le fichier

    :CU: Aucune

    Exemples: Aucun pour l'instant, nous crérons un fichier de base à l'aide de LOREMIPSUM, pour les testes lors des prochaines scéances.
    """
    try:
        list = []
        with open(fichier,'r') as fichier:
            list = [ligne.strip() for ligne in fichier if ligne.strip() != ""]
        return list
    except Exception as e:
        log(str(e))

def ls(fichier = file_name):
    """
    Imprime toutes les tâches (via pretty) contenues dans fichier.

    :param fichier: (str) Valeur par defaut: to-do.txt
    :return: Aucun
    """
    try:
        taches = lire(fichier)
        for tache in taches:
            pretty(tache)
    except Exception as e:
        log(str(e))

def add(ligne = ligne_defaut, fichier = file_name):
    """
    Ajoute une ligne dans le fichier voulu

    :param ligne: (str) Du texte (les \n etc seront supprimer automatiquement)
    :return: Aucun

    :CU: Le fichier en question existe
    """
    try:
        with open(fichier, 'a') as taches:
            taches.write(' '.join(ligne.split()) + '\n')
    except Exception as e:
        log(str(e))

def remove(n, fichier = file_name):
    """
    Supprime une ligne du fichier

    :param n: (int) Un entier
    :return: Aucun

    :CU: J'espère que le ligne existe petit malin !
    """
    try:
        if n <= longueur_fichier(fichier):
            with open(fichier, "r") as f_taches:
                taches = f_taches.readlines()
                taches.remove(taches[n])
            print(taches)
            with open(fichier, "w") as f_taches:
                f_taches.writelines(taches)
##                f_taches.write('\n'.join(taches))
        else:
            log("Le programme a essayer de supprimer une ligne " + str(n) + " qui n'existait pas dans le fichier " + str(fichier) + " ", "LectureFichier")
    except Exception as e:
        log(str(e))

def sort(cle = "N", fichier = file_name):
    """
    Fonction à finir

    :param cle: (str) Une clé, N, X, completionDate, creationDate, priority, context, project, text, title
    :param fichier: (str) Un fichier, par defaut to-do.txt
    """
    try:
        with open(fichier, 'r') as f_taches:
            list = [parser_tache(n) for tache in f_taches]

        list = sorted(list, key=lambda k: k[cle])

        with open(fichier, 'w') as f_taches:
            f_taches.write("\n".join(unparser_tache))
    except Exception as e:
        log(str(e))

##                _                  ##
##     /\        | |                 ##
##    /  \  _   _| |_ _ __ ___  ___  ##
##   / /\ \| | | | __| '__/ _ \/ __| ##
##  / ____ \ |_| | |_| | |  __/\__ \ ##
## /_/    \_\__,_|\__|_|  \___||___/ ##
##                                   ##
##                                   ##

def longueur_fichier(fichier):
    """
    Renvoi le nombre de ligne d'un fichier

    :param fichier: (str) Valeur par defaut: to-do.txt
    :return: (int) le nombre de ligne

    :CU: Aucune
    """
    try:
        with open(fichier) as f_taches:
            for i, l in enumerate(f_taches):
                pass
        return i + 1
    except Exception as e:
        log(str(e))

def log(txt, type= "Python"):
    """
    Fonction qui ecris les erreur dans un fichier log.txt, dans le but de garder un historique des erreurs

    :param txt: (str) L'erreur en question
    :return: Aucun

    :CU: Ne pas utiliser pas vous même, fonction programme
    """
    with open("log.txt", "a") as logs:
        import datetime
        heure = datetime.datetime.now()
        logs.write("Erreur : (" + type + ")  " + str(heure) + "  " + txt + "\n")
