##.___                                    ##
## /   \   ___  .___    ____   ___  .___  ##
## |,_-'  /   ` /   \  (     .'   ` /   \ ##
## |     |    | |   '  `--.  |----' |   ' ##
## /     `.__/| /     \___.' `.___, /     ##
##                                        ##

ligne_defaut = "X (A) 2018-15-02 2018-03-12 Les pommes c'est quand même sacrément bon +Tag_Projet @Tag_Context due:2018-03-12 #ti: Titre meta: pomme:true"
ligne_defaut1 = "2018-15-02 (A) TXT +Tag_Projet @Tag_Context due:XXXX-XX-XX #ti: Titre"
ligne_defaut2 = "X 2018-03-12 TXT +Tag_Projet @Tag_Context due:XXXX-XX-XX #ti: Titre"
ligne_defaut3 = "X 2018-03-12 TXT +Tag_Projet @Tag_Context due:XXXX-XX-XX #ti: Titre meta: test number 1"
ligne_defaut4 = "X 2018-03-12 meta: test pomme"

##    ┌┬┐┬─┐┬  ┌┬┐┌─┐┌─┐  ┌┬┐┌─┐┌┐┌┌┐┌┌─┐┌─┐┌─┐    ##
##     │ ├┬┘│   ││├┤ └─┐   │││ │││││││├┤ ├┤ └─┐    ##
##     ┴ ┴└─┴  ─┴┘└─┘└─┘  ─┴┘└─┘┘└┘┘└┘└─┘└─┘└─┘    ##

def parser(ligne = ligne_defaut):
    ligne = ligne.strip().lower().split()
    t = {}
    i = 0
    a = ''
    b = ''
    while i in range(len(ligne)):
        element = ligne[i]
        element_type = type_predata(element)        
        if element_type != 'Autres':
            ligne.remove(element)
            if element_type in t.keys():
                t[element_type] += "  " + element
            else:
                t[element_type] = element
        else:
            i+=1   
    ligne = [f if ':' not in f else f.split(':') for f in ligne]
    ligne = type_postdata(ligne)
    return merge_dict(t, ligne)

def type_predata(e):
    prio = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)', '(i)', '(j)', '(k)', '(l)', '(m)', '(n)', '(o)', '(p)', '(q)', '(r)', '(s)', '(t)', '(u)', '(v)', '(w)', '(x)', '(y)', '(z)', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '(A)', '(B)', '(C)', '(D)', '(E)', '(F)', '(G)', '(H)', '(I)', '(J)', '(K)', '(L)', '(M)', '(N)', '(O)', '(P)', '(Q)', '(R)', '(S)', '(T)', '(U)', '(V)', '(W)', '(X)', '(Y)', '(Z)'] 
    prio = ''.join(prio)
    type = {'x': 'completion', prio:'priority', '@':'context_tag', '+':'project_tag', 'due':'due_date', 'meta':'meta', '#ti':'titre'}
    if e == 'x':
        return 'completion'
    if e in prio:
        return 'priority'
    import re
    r = re.compile('\d{4}-\d{2}-\d{2}')
    if r.match(e) is not None:
        return 'date'
    if '@' in e:
        return 'context_tag'
    if '+' in e:
        return 'project_tag'
    if 'due' in e:
        return 'due_date'
    else:
        return 'Autres'
    
def type_postdata(ligne):
    dict_temp = {}
    str_temp = []
    ligne_temp = []
    
    for i in range(len(ligne)-1, -1, -1):
        element = ligne[i]
        if type(element) != str:
            e0 = element[0]
            e1 = element[1]
            if len(e0) != 0 and len(e1) != 0:
                dict_temp[e0] = e1
                element = ''
            if len(e0) != 0 and len(e1) == 0:
                if len(dict_temp.keys()) != 0:
                    element[1] = dict_temp
                    dict_temp = {}
                elif len(str_temp) != 0:
                    
                    element[1] = join_reverse(str_temp)
                    str_temp = []
        else: 
            str_temp.append(element)
            element = ''
        
        if element != '':
            ligne_temp.append(element) 

    if type(ligne[0]) == str:    
        str_temp.append(ligne[0])
        
    ligne_temp.reverse()
    dict_temp = {clé: valeur for (clé, valeur) in ligne_temp}
    dict_temp['text'] = join_reverse(str_temp)
    return dict_temp
    
##    ┌─┐┬ ┬┌┬┐┬─┐┌─┐┌─┐    ##
##    ├─┤│ │ │ ├┬┘├┤ └─┐    ##
##    ┴ ┴└─┘ ┴ ┴└─└─┘└─┘    ##
 
def merge_dict(d1, d2):
    d = d1.copy()
    d.update(d2)
    return d
    
def join_reverse(str):
    str.reverse()
    str = ' '.join(str)
    return str